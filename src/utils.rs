#[derive(Debug, Clone, Copy)]
pub struct Color {
    pub r: f32,
    pub g: f32,
    pub b: f32,
    pub a: f32
}

pub const BLACK: Color = Color::new_rgb(0.0, 0.0, 0.0);

impl Color {
    pub const fn new_rgb(r: f32, g: f32, b: f32) -> Self {
        Self {r, g, b, a: 1.0 }
    }

    pub fn new_rgba(r: f32, g: f32, b: f32, a: f32) -> Self {
        Self { r, g, b, a }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Position {
    pub x: f32,
    pub y: f32
}

#[derive(Debug, Clone, Copy)]
pub enum Alignment {
    Center,
    Start,
    End
}

#[derive(Debug, Clone, Copy)]
pub enum SizeType {
    Fill,
    Content
}

pub fn wunit_to_px(units: f32, width: u32) -> f32 {
    units * (width as f32 / 30.)
}

pub fn hunit_to_px(units: f32, height: u32) -> f32 {
    units * (height as f32 / 30.)
}

pub fn px_to_wunit(px: u32, width: u32) -> f32 {
    px as f32 / (width as f32 / 30.)
}

pub fn px_to_hunit(px: u32, height: u32) -> f32 {
    px as f32 / (height as f32 / 30.)
}