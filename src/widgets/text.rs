use crate::{utils::Color, utils::BLACK};

#[derive(Clone, Copy, Debug)]
pub struct TextStyle {
    pub scale: f32,
    pub color: Color,
}

impl Default for TextStyle {
    fn default() -> Self {
        Self {
            scale: 16.0,
            color: BLACK,
        }
    }
}

impl TextStyle {
    
    pub fn new(scale: f32, color: Color) -> TextStyle {
        Self {
            scale,
            color
        }
    }

    pub fn default_scaled(scale: f32) -> Self {
        let style = TextStyle::new(scale, BLACK);
        style
    }
}

#[derive(Clone, Debug)]
pub struct Text {
    pub text: String,
    pub style: TextStyle
}

impl Text {
    pub fn new(text: String) -> Self {
        Self {
            text,
            style: TextStyle::default(),
        }
    }
    
    pub fn with_style(text: String, style: TextStyle) -> Self {
        Self {
            text,
            style: style
        }
    }

    pub fn style(mut self, style: TextStyle) -> Self {
        self.style = style;
        self
    }

}
