use crate::{utils::{Position, px_to_wunit, px_to_hunit}, renderer::Renderer};
use crate::widgets::text::{Text, TextStyle};
use super::{Widget, WidgetMesh};

pub struct Button {
    pub text: Text,
    pub size: (f32, f32),
    pub position: Position,
    on_clicked: Option<Box<dyn Fn()>>
}

impl Button {
    pub fn new() -> Self {
        Self {
            // Replaced with new constructor
            text: Text::with_style(String::new(), TextStyle::default_scaled(32.0)),
            // text: Text {
            //     text: String::new(),
            //     style: TextStyle {
            //         scale: 32.,
            //         color: Color::new_rgb(0., 0., 0.),
            //     },
            size: (0., 0.),
            position: Position { x: 0., y: 0. },
            on_clicked: None
        }
         
    }

    pub fn text(self, text: Text) -> Self {
        Self {
            text,
            size: self.size,
            position: self.position,
            on_clicked: self.on_clicked
        }
    }

    pub fn on_clicked(mut self, on_clicked: Box<dyn Fn()>) -> Self {
        self.on_clicked = Some(on_clicked);
        self
    }

    pub fn set_text(&mut self, text: Text) {
        self.text = text;
    }

    pub fn get_text(&self) -> Text {
        self.text.clone()
    }
}

impl Widget for Button {
    fn get_mesh(&self) -> Option<&WidgetMesh> {
        None
    }

    fn get_mesh_mut(&mut self) -> Option<&mut WidgetMesh> {
        None
    }

    fn init(&mut self, renderer: &mut Box<dyn Renderer>) {
        let (width, height) = renderer.get_text_size(self.text.clone());

        self.size = (px_to_wunit(width as u32, renderer.get_size().width), px_to_hunit(height as u32, renderer.get_size().height));
    }

    fn draw(&self, renderer: &mut Box<dyn Renderer>) {

        renderer.draw_text(self.text.clone(), self.position);
    }

    fn get_size(&self) -> (f32, f32) {
        self.size
    }

    fn set_position(&mut self, position: Position) {
        self.position = position;
    }

    fn get_position(&self) -> Position {
        self.position
    }

    fn click(&mut self, _pos: (f32, f32)) {
        if let Some(callback) = &self.on_clicked {
            callback();
        }
    }
}