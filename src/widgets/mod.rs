use cgmath::Vector3;
use wgpu::util::DeviceExt;
use text::{Text, TextStyle};
use crate::{
    renderer::{Renderer, Transform, TransformUniform, Vertex},
    utils::{px_to_hunit, px_to_wunit, Color, Position},
};

pub mod column;
pub mod row;
pub mod button;
pub mod text;
pub struct WidgetMesh {
    vertex_buffer: Option<wgpu::Buffer>,
    index_buffer: Option<wgpu::Buffer>,
    pub transform_bind_group: Option<wgpu::BindGroup>,
    transform_uniform: TransformUniform,
    indices: Vec<u16>,
    vertices: Vec<Vertex>,
    transform: Transform,
    transform_buffer: Option<wgpu::Buffer>,
}

impl WidgetMesh {
    pub fn new() -> Self {
        Self {
            vertex_buffer: None,
            index_buffer: None,
            transform_bind_group: None,
            transform_uniform: TransformUniform::new(),
            indices: Vec::new(),
            vertices: Vec::new(),
            transform: Transform::new(Vector3::new(0., 0., 0.)),
            transform_buffer: None,
        }
    }

    pub fn set_vertices(&mut self, vertices: Vec<Vertex>) {
        self.vertices = vertices;
    }

    pub fn set_indices(&mut self, indices: Vec<u16>) {
        self.indices = indices;
    }

    pub fn init(&mut self, renderer: Box<&dyn Renderer>) {
        self.vertex_buffer = Some(renderer.get_device().create_buffer_init(
            &wgpu::util::BufferInitDescriptor {
                label: Some("Widget mesh > Vertex Buffer"),
                contents: bytemuck::cast_slice(self.vertices.as_slice()),
                usage: wgpu::BufferUsages::VERTEX,
            },
        ));

        self.index_buffer = Some(renderer.get_device().create_buffer_init(
            &wgpu::util::BufferInitDescriptor {
                label: Some("Widget Mesh > Index Buffer"),
                contents: bytemuck::cast_slice(self.indices.as_slice()),
                usage: wgpu::BufferUsages::INDEX,
            },
        ));

        self.transform_uniform.update_transformation(self.transform);

        self.transform_buffer = Some(renderer.get_device().create_buffer_init(
            &wgpu::util::BufferInitDescriptor {
                label: Some("Widget Mesh > Transform Buffer"),
                contents: bytemuck::cast_slice(&[self.transform_uniform]),
                usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
            },
        ));

        self.transform_bind_group = Some(renderer.get_device().create_bind_group(
            &wgpu::BindGroupDescriptor {
                label: Some("Widget Mesh > Transform Bind Group"),
                layout: renderer.get_transform_layout(),
                entries: &[wgpu::BindGroupEntry {
                    binding: 0,
                    resource: self.transform_buffer.as_ref().unwrap().as_entire_binding(),
                }],
            },
        ));
    }

    pub fn get_vertex_buffer(&self) -> Option<&wgpu::Buffer> {
        self.vertex_buffer.as_ref()
    }

    pub fn get_index_buffer(&self) -> Option<&wgpu::Buffer> {
        self.index_buffer.as_ref()
    }

    pub fn get_indices(&self) -> Vec<u16> {
        self.indices.clone()
    }
}

pub trait Widget {
    /// This get called every time the application needs to be redrawed, the `renderer` parameter can be used for rendering using the application backend
    fn draw(&self, _renderer: &mut Box<dyn Renderer>) {}

    /// This is used by the renderer to draw the mesh for the widget
    fn get_mesh(&self) -> Option<&WidgetMesh> {
        None
    }

    fn get_mesh_mut(&mut self) -> Option<&mut WidgetMesh> {
        None
    }

    fn set_parent_size(&mut self, _size: (f32, f32)) {}

    /// This get called when the widget is created
    fn init(&mut self, renderer: &mut Box<dyn Renderer>) {
        if let Some(mesh) = self.get_mesh_mut() {
            mesh.init(Box::new(renderer.as_ref()));
        }
    }

    fn set_size(&mut self, _size: (f32, f32)) {}

    fn mouse_move(&mut self, _pos: (f32, f32)) {}
    fn click(&mut self, _pos: (f32, f32)) {}
    fn hover(&mut self, _pos: (f32, f32)) {}

    /// Returns the size of the widget in units
    fn get_size(&self) -> (f32, f32) {
        (0., 0.)
    }

    fn get_size_from(&self, _bounds: (f32, f32)) -> (f32, f32) { (0., 0.) }

    /// Set widget position
    fn set_position(&mut self, _position: Position) {}

    /// Get widget position
    fn get_position(&self) -> Position {
        Position { x: 0., y: 0. }
    }

    fn set_screen_size(&mut self, _screen_size: (f32, f32)) {}

}

/// Simple widget that can display text
#[derive(Debug)]
pub struct Label {
    pub text: Text,
    pub size: (f32, f32),
    pub position: Position,
}

impl Default for Label {

    fn default() -> Self {
        let style = TextStyle::default_scaled(32.0);
        Self {
            text: Text::with_style(String::new(), style),
            size: (0.0, 0.0),
            position: Position { x: 0.0, y: 0.0 }
        }
    }
}
impl Label {
    
    pub fn new(text: Text) -> Self {
        
        let label = Self::default().with_text(text);
        label

        // This was replaced with better constructors
        
       // Self {   
            // text: Text {
            //     text: String::new(),
            //     style: TextStyle {
            //         scale: 32.,
            //         color: Color::new_rgb(0., 0., 0.),
            //     },
            // },
            // size: (0., 0.),
            // position: Position { x: 0., y: 0. },
       // }
    }

    pub fn with_text(&self, text: Text) -> Self {
        Self {
            text: text,
            size: self.size,
            position: self.position,
        }
    }

    pub fn set_text(&mut self, text: Text) {
        self.text = text;
    }

    pub fn get_text(&self) -> Text {
        self.text.clone()
    }
}

impl Widget for Label {
    fn get_mesh(&self) -> Option<&WidgetMesh> {
        None
    }

    fn get_mesh_mut(&mut self) -> Option<&mut WidgetMesh> {
        None
    }

    fn init(&mut self, renderer: &mut Box<dyn Renderer>) {
        let (width, height) = renderer.get_text_size(self.text.clone());

        self.size = (px_to_wunit(width as u32, renderer.get_size().width), px_to_hunit(height as u32, renderer.get_size().height));
    }

    fn draw(&self, renderer: &mut Box<dyn Renderer>) {

        renderer.draw_text(self.text.clone(), self.position);
    }

    fn get_size(&self) -> (f32, f32) {
        self.size
    }

    fn set_position(&mut self, position: Position) {
        self.position = position;
    }

    fn get_position(&self) -> Position {
        self.position
    }
}
