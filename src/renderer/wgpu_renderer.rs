//TODO: create a function to draw quads

use std::{
    borrow::{Borrow, BorrowMut},
    cell::RefCell,
    rc::Rc,
};

use ab_glyph::FontArc;
use futures::{executor::LocalPool, task::SpawnExt};
use wgpu::{
    util::{StagingBelt, DeviceExt}, Backends, Features, Instance, Limits, SurfaceConfiguration, TextureUsages,
};
use wgpu_glyph::{FontId, GlyphBrush, GlyphBrushBuilder, GlyphCruncher, Section, Text};
use winit::dpi::PhysicalSize;

use crate::{
    assets::{fonts::get_font, shaders::get_wgpu_shader_module},
    utils::{hunit_to_px, wunit_to_px, Position},
    widgets::Widget,
};

use super::{OrthographicProjection, OrthographicProjectionUniform, Renderer, Vertex};

pub struct WgpuRenderer {
    surface: wgpu::Surface,
    device: wgpu::Device,
    queue: wgpu::Queue,
    render_pipeline: wgpu::RenderPipeline,
    config: wgpu::SurfaceConfiguration,
    pub size: PhysicalSize<u32>,
    widget_queue: Vec<Rc<RefCell<Box<dyn Widget>>>>,
    transform_bind_group_layout: wgpu::BindGroupLayout,
    staging_belt: wgpu::util::StagingBelt,
    local_pool: futures::executor::LocalPool,
    local_spawner: futures::executor::LocalSpawner,
    glyph_brush: GlyphBrush<()>,
    projection: OrthographicProjection,
    projection_uniform: OrthographicProjectionUniform,
    projection_buffer: wgpu::Buffer,
    projection_bind_group: wgpu::BindGroup,
}

impl WgpuRenderer {
    pub fn new(window: &winit::window::Window) -> Self {
        let size = window.inner_size();
        let instance = Instance::new(Backends::all());
        let surface = unsafe { instance.create_surface(window) };
        let adapter = pollster::block_on(instance.request_adapter(&wgpu::RequestAdapterOptions {
            power_preference: wgpu::PowerPreference::default(),
            force_fallback_adapter: false,
            compatible_surface: Some(&surface),
        }))
        .unwrap();

        let (device, queue) = pollster::block_on(adapter.request_device(
            &wgpu::DeviceDescriptor {
                label: Some("Device"),
                features: Features::empty(),
                limits: Limits::default(),
            },
            None,
        ))
        .unwrap();

        let config = SurfaceConfiguration {
            usage: TextureUsages::RENDER_ATTACHMENT,
            format: surface.get_preferred_format(&adapter).unwrap(),
            width: window.inner_size().width,
            height: window.inner_size().height,
            present_mode: wgpu::PresentMode::Fifo,
        };

        let transform_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                label: Some("Transform Bind Group Layout"),
                entries: &[wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStages::VERTEX,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                }],
            });

        let projection_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                label: Some("Projection Bind Group Layout"),
                entries: &[wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStages::VERTEX,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                }],
            });

        let shader = get_wgpu_shader_module(&device, "main").unwrap();

        let render_pipeline_layout =
            device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("Render Pipeline"),
                bind_group_layouts: &[&transform_bind_group_layout, &projection_bind_group_layout],
                push_constant_ranges: &[],
            });

        let render_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some("Render Pipeline"),
            layout: Some(&render_pipeline_layout),
            vertex: wgpu::VertexState {
                module: &shader,
                entry_point: "vs_main",
                buffers: &[Vertex::desc()],
            },
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleList,
                strip_index_format: None,
                front_face: wgpu::FrontFace::Ccw,
                cull_mode: Some(wgpu::Face::Back),
                unclipped_depth: false,
                polygon_mode: wgpu::PolygonMode::Fill,
                conservative: false,
            },
            depth_stencil: None,
            multisample: wgpu::MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: false,
            },
            fragment: Some(wgpu::FragmentState {
                module: &shader,
                entry_point: "fs_main",
                targets: &[wgpu::ColorTargetState {
                    format: config.format,
                    blend: Some(wgpu::BlendState::REPLACE),
                    write_mask: wgpu::ColorWrites::ALL,
                }],
            }),
            multiview: None,
        });

        surface.configure(&device, &config);

        let staging_belt = StagingBelt::new(1024);
        let local_pool = LocalPool::new();
        let local_spawner = local_pool.spawner();

        let glyph_brush =
            GlyphBrushBuilder::using_font(get_font().unwrap()).build(&device, config.format);

        let projection = OrthographicProjection::new(size.width as f32, size.height as f32);

        let mut projection_uniform = OrthographicProjectionUniform::new();
        projection_uniform.update_projection(projection.clone());

        let projection_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Projection Buffer"),
                contents: bytemuck::cast_slice(&[projection_uniform]),
                usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
        });

        let projection_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("Projection Bind Group"),
            layout: &projection_bind_group_layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: projection_buffer.as_entire_binding(),
            }],
        });

        Self {
            surface,
            device,
            queue,
            render_pipeline,
            config,
            size,
            widget_queue: Vec::new(),
            transform_bind_group_layout,
            staging_belt,
            local_pool,
            local_spawner,
            glyph_brush,
            projection,
            projection_uniform,
            projection_bind_group,
            projection_buffer
        }
    }

    fn font_id(&mut self, font: FontArc) -> FontId {
        self.glyph_brush.add_font(font)
    }

    fn text_size(&mut self, text: crate::widgets::text::Text, font: FontArc) -> (f32, f32) {
        let section = wgpu_glyph::Section {
            bounds: (self.size.width as f32, self.size.height as f32),
            text: vec![Text {
                text: &text.text,
                scale: text.style.scale.into(),
                font_id: self.font_id(font),
                extra: wgpu_glyph::Extra::default(),
            }],
            ..Default::default()
        };

        if let Some(bounds) = self.glyph_brush.borrow_mut().glyph_bounds(section) {
            (bounds.width().ceil(), bounds.height().ceil())
        } else {
            (0., 0.)
        }
    }
}

impl Renderer for WgpuRenderer {
    fn render(&mut self) -> Result<(), wgpu::SurfaceError> {
        let output = self.surface.get_current_texture()?;
        let view = output
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());
        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Render Encoder"),
            });

        let widgets: Vec<_> = self
            .widget_queue
            .iter()
            .map(|rc| rc.as_ref().borrow())
            .collect();

        let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: Some("Render Pass"),
            color_attachments: &[wgpu::RenderPassColorAttachment {
                view: &view,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(wgpu::Color {
                        r: 1.0,
                        g: 1.0,
                        b: 1.0,
                        a: 1.0,
                    }),
                    store: true,
                },
            }],
            depth_stencil_attachment: None,
        });

        render_pass.set_pipeline(&self.render_pipeline);

        for widget in widgets.iter() {
            let mesh = widget.as_ref().borrow();
            if let Some(mesh) = mesh.get_mesh() {
                render_pass.set_vertex_buffer(0, mesh.get_vertex_buffer().unwrap().slice(..));
                render_pass.set_index_buffer(
                    mesh.get_index_buffer().unwrap().slice(..),
                    wgpu::IndexFormat::Uint16,
                );
                render_pass.set_bind_group(0, mesh.transform_bind_group.as_ref().unwrap(), &[]);
                render_pass.draw_indexed(0..mesh.get_indices().len() as u32, 0, 0..1);
            }
        }

        drop(render_pass);
        drop(widgets);

        self.glyph_brush
            .draw_queued_with_transform(
                &self.device,
                &mut self.staging_belt,
                &mut encoder,
                &view,
                wgpu_glyph::orthographic_projection(self.config.width, self.config.height),
            )
            .unwrap();

        self.staging_belt.finish();
        self.queue.submit(std::iter::once(encoder.finish()));
        output.present();

        self.widget_queue.clear();

        self.local_spawner
            .spawn(self.staging_belt.recall())
            .unwrap();
        self.local_pool.run_until_stalled();

        Ok(())
    }

    fn resize(&mut self, new_size: PhysicalSize<u32>) {
        self.size = new_size;
        self.projection =
            OrthographicProjection::new(self.size.width as f32, self.size.height as f32);
        self.projection_uniform.update_projection(self.projection);
        self.queue.write_buffer(&self.projection_buffer, 0, bytemuck::cast_slice(&[self.projection_uniform]));
        self.config.width = new_size.width;
        self.config.height = new_size.height;

        self.surface.configure(&self.device, &self.config);
    }

    fn draw_widget(&mut self, widget: Rc<RefCell<Box<dyn crate::widgets::Widget>>>) {
        let widget = widget.clone();
        self.widget_queue.push(widget);
    }

    fn draw_text(&mut self, text: crate::widgets::text::Text, position: Position) {
        let text = Text::new(&text.text)
            .with_color([
                text.style.color.r,
                text.style.color.g,
                text.style.color.b,
                text.style.color.a,
            ])
            .with_scale(text.style.scale);

        self.glyph_brush.queue(Section {
            screen_position: (
                wunit_to_px(position.x, self.size.width),
                hunit_to_px(position.y, self.size.height),
            ),
            bounds: (self.size.width as f32, self.size.height as f32),
            text: vec![text],
            ..Section::default()
        });
    }

    fn get_size(&self) -> PhysicalSize<u32> {
        self.size
    }

    fn get_device(&self) -> &wgpu::Device {
        &self.device
    }

    fn get_transform_layout(&self) -> &wgpu::BindGroupLayout {
        &self.transform_bind_group_layout
    }

    fn get_text_size(&mut self, text: crate::widgets::text::Text) -> (f32, f32) {
        self.text_size(text, get_font().unwrap())
    }
}
