use std::{fs, io};

pub fn get_wgpu_shader_module(device: &wgpu::Device, name: &str) -> io::Result<wgpu::ShaderModule> {
    let source = fs::read_to_string(format!("assets/shaders/{}.wgsl", name))?;

    Ok(device.create_shader_module(&wgpu::ShaderModuleDescriptor {
        label: Some(name),
        source: wgpu::ShaderSource::Wgsl(source.into()),
    }))
}